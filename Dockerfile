FROM golang:1.15-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
      PATH_TO_MODULE=`go list -m` && \
      go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer

FROM mono:6.12

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-3.5.3}

# Partially from https://docs.microsoft.com/en-au/dotnet/core/install/linux-package-manager-debian9
RUN apt-get update && \
      apt-get install -y apt-transport-https gpg wget zip && \
      wget -O- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg && \
      mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/ && \
      wget https://packages.microsoft.com/config/debian/9/prod.list && \
      mv prod.list /etc/apt/sources.list.d/microsoft-prod.list && \
      chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg && \
      chown root:root /etc/apt/sources.list.d/microsoft-prod.list && \
      apt-get update && \
      apt-get install -y dotnet-sdk-3.1

RUN mkdir /root/security-code-scan && \
      cd /root/security-code-scan/ && \
      wget https://github.com/security-code-scan/security-code-scan/releases/download/$SCANNER_VERSION/SecurityCodeScan.Vsix.vsix && \
      unzip SecurityCodeScan.Vsix.vsix && \
      rm SecurityCodeScan.Vsix.vsix

COPY --from=build /analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]

package project

import (
	"bufio"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"strings"
)

const analyzersXML = `
  <ItemGroup>
    <Analyzer Include="/root/security-code-scan/SecurityCodeScan.dll" />
    <Analyzer Include="/root/security-code-scan/YamlDotNet.dll" />
  </ItemGroup>
`

// InsertAnalyzersToProjFile inserts SecurityCodeScan analyzer at the end of csproj file
func InsertAnalyzersToProjFile(path string) error {
	lines, err := file2lines(path)
	if err != nil {
		return err
	}

	var index = -1

	for i, line := range lines {
		if strings.Trim(line, "\n") == "</Project>" {
			index = i
		}
	}

	if index == -1 {
		return errors.New("</Project> ending tag not found")
	}
	fileContent := ""
	for i, line := range lines {
		if i == index {
			fileContent += analyzersXML
		}
		fileContent += line
		fileContent += "\n"
	}

	return ioutil.WriteFile(path, []byte(fileContent), 0644)
}

// file2lines converts a file to an array of lines (string)
func file2lines(filePath string) ([]string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	return linesFromReader(f)
}

// linesFromReader converts reader to an array of lines (string)
func linesFromReader(r io.Reader) ([]string, error) {
	var lines []string
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return lines, nil
}
